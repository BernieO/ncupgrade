#!/usr/bin/env bash

# hook_script_example.sh: example hook-script for 'ncupgrade' when restoring backups


# If the environment differs from the location where a backup was taken and
# the location to where a backup will be restored, a hook script can be
# used to adapt the backup to the new environment (e.g. changed passwords, etc.).

# Copy this example script to a location of your liking and pass the path via
# option -hs|--hook-script to 'ncupgrade'.

# The commands within the hook script will then be processed when restoring a
# backup, namely after restoring nextclouds files (folder + data directory)
# and just before restoring the database.

# Do not alter any functions or these global variables, as the script then might not work:
# ${adjust_permissions_datadir}, ${adjust_permissions_fast}, ${backup_dir}, ${compression_method}
# ${configphp}, ${data_dir_missing}, ${htuser}, ${htgroup}, ${nc_backup_sql}, ${nc_data_dir}
# ${nextcloud_dir}, ${permf}, ${permd}, ${php_command}, ${scan_files}, ${temp_dir}


# Global variables as well as functions declared in 'ncupgrade' can be accessed
# from within the hook script. Examples:
printf '%s\n' "+  temporary_directory:           '${temp_dir}'"
printf '%s\n' "+  database backup to be used:    '${backup_dir}/${nc_backup_sql}'"
database_name="$(getvalue_from_configphp "dbname")"
path_without_trailing_slashes="$(remove_trailing_slashes "/path////")"
printf '%s\n' "+  database name:                 '${database_name}'"
printf '%s\n' "+  path without trailing slashes: '${path_without_trailing_slashes}'"


# Be aware that 'ncupgrade' is running with shell attributes '-euo pipefail' and
# thus will exit:
#    - when a simple command in a command list exits non-zero
#    - when it finds an unset variable
#    - when any command in a pipe fails


# Do not use the command 'exit' to exit the hook script. As the hook script
# is sourced, this will exit 'ncupgrade'.

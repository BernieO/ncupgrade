# ncupgrade

This Bash script upgrades a local Nextcloud installation based on the [Nextcloud documentation for manual upgrades](https://github.com/nextcloud/documentation/blob/master/admin_manual/maintenance/manual_upgrade.rst).  
Before the upgrade a backup of the running Nextcloud instance will be created [(also based on the official documentation)](https://github.com/nextcloud/documentation/blob/master/admin_manual/maintenance/backup.rst).  
With option `-ob` this script may also be used to only backup Nextcloud (no upgrade will be performed with this option).  
With option `-rb` a backup previously created with this script can easily be restored.  

**In any case: use at own risk!**

## Contents
- [Requirements](#requirements)
- [Quick Guide](#quick-guide)
- [Migrating from GitHub to Codeberg](#migrating-from-github-to-codeberg)
- [Options](#options)
- [Get in touch](#get-in-touch)
- [Donations](#donations)
- [License](#license)

## Requirements

- local Nextcloud installation
- packages `curl`, `sudo` and `gnupg`
- it is best to run this script with root privileges, though it may also work if run as webserver user (in this case the webserver obviously can't be stopped during upgrade)
- if you want to stop the webserver during upgrade process (like recommended in the documentation), notice:
   - the script needs then to be run with root privileges
   - the script uses a `systemctl` call to stop and start the webserver
   - use option `-w WEBSERVER` and replace WEBSERVER with the service name of your webserver
   - or use option `-k`, if you don't want to stop the webserver (or you can't stop it for whatever reason)

## Quick Guide

1. Clone the repository to your server and enter the repo:  
   `git clone https://codeberg.org/BernieO/ncupgrade.git`  
   `cd ncupgrade`  

2. Run the script with root privileges and following arguments:
   - the path to your Nextcloud installation (must be first argument)
   - option -w and your webservers service name (to stop and start the webserver during upgrade)
   - for example: `sudo ./ncupgrade /path/to/nextcloud -w nginx`  
&nbsp;

3. Take into account that the different operations when upgrading, backing up or restoring a backup may take some time depending on the size of your installation.

4. Check the output of the script carefully for errors. For troubleshooting [check this out](https://github.com/nextcloud/documentation/blob/master/admin_manual/maintenance/manual_upgrade.rst#troubleshooting)  
   Prior to upgrading, the script creates a backup of the Nextcloud instance which may be used for restoring the previous version (you may find the according command at the end of the output of the script).

5. Login to your Nextcloud webinterface and reenable all compatible 3rd party apps.

## Migrating from GitHub to Codeberg

To migrate from GitHub to Codeberg, it is easiest to rename the folder containing the GitHub repository and then follow the [Quick Guide](#quick-guide) again.
If the backup folder resides in the folder with the GitHub repository, move it to the new location.

## Options

All options need to be specified as command line arguments.  
It is mandatory to pass as very first argument the path to your Nextcloud instance as well as passing option `-w WEBSERVER` or `-k` to the script.  
If run with option `-ob` or `-rb` the options `-k`/`-w WEBSERVER` will be ignored.

```
Usage: ./ncupgrade [DIRECTORY] [option [argument]] [option [argument]] [option [argument]] ...

Arguments in capital letters to options are mandatory.
Paths (FILE / DIRECTORY) are absolute paths or relative paths to working directory.

-apf | --adjust-permissions-fast
       Use a different approach than outlined in the official Nextcloud documentation to adjust file permissions
       after upgrading or restoring from a backup. The result is the same, but faster achieved. Default is to
       use the traditional, slower approach according to the official documentation of Nextcloud.
       This option will be ignored, if the script is not being run as root.
-bd | --backup-directory DIRECTORY
       Use directory DIRECTORY to store backups.
       If this option is not given, folder 'nextcloud_backups/' in script's directory is created and used.
-ea | --exclude-apps
       Do not copy missing apps from the previous installation to the upgraded installation.
-ed | --exclude-data-directory
       In case the data directory is located outside of Nextclouds directory it can be excluded from backup with
       this option. The update will not touch the data directory. This option will be ignored, if the data
       directory resides inside Nextclouds directory.
-et | --exclude-themes
       Do not copy missing themes from the previous installation to the upgraded installation.
-h  | --help
       Print version number and a short help text
-hs | --hook-script FILE
       Process commands from FILE when restoring a backup (just before restoring the database).
       If the environments from the location where a backup was taken and the location to where
       a backup will be restored are different, with this option a hook script can be passed to
       'ncupgrade' to adapt the backup to the new environment (e.g. changed passwords, etc.).
       This option will be ignored, if not set together with option -rb.
       For detailed information about FILE see 'hook_script_example.sh'.
-k  | --keep-webserver-running
       Don't stop webserver during upgrade. Though the Nextcloud ducumentation recommends to stop the webserver
       during upgrade, this might not always be possible (for example in shared hosting environments or if other
       webservices running on the same webserver need to stay online).
       Either this option or -w WEBSERVER need to be passed to the script.
-l  | --less-restrictive-permissions
       after upgrading or restoring from a backup ncupgrade adjusts access permissions for Nextclouds directories
       and files according to the documentation to 750 (directories) and 640 (files). In certain environments
       (e.g. shared hosting at uberspace) this might be to restrictive resulting in the webserver not being able
       to read and serve files. Use this option to set less restrictive permissions (755 for directories and
       644 for files).
-nb | --no-backup
       do not backup the Nextcloud instance before upgrading.
       It is not recommended to use this option (except for testing purposes).
-php| --php-engine "COMMAND [OPTIONS]"
       as default ncupgrade uses the command 'php' to run php scripts. This option gives the possibility to use a
       different php engine. Optionally, also options for the php engine can be added.
       example: ./ncupgrade "/path/to/nextcloud" -php "/usr/bin/php74 -d memory_limit=1024M"
-ob | --only-backup
       only create a backup of Nextcloud and then quit. No Upgrade will be done.
       When run with this option, options -k/-w will be ignored
-rb | --restore-backup
       restores a backup which was created with ncupgrade. The script will look for backups in the
       backup-directory given via option -bd, or in folder 'nextcloud_backups/' in script's directory.
       NOTE: if the according database user does not exist yet in the database, it will not be created.
-t  | --tar
       use simple tar to create the backup files as uncompressed archives.
-utv| --upgrade-to-version X.X.X
       Upgrade to specific version X.X.X instead of upgrading to the latest available possible.
       Downgrading as well as skipping major releases is not supported.
       For normal operation this option is not neccessary, since ncupgrade looks automatically for
       the latest possible available upgrade.
-w  | --webserver WEBSERVER
       Stop service WEBSERVER during upgrade with systemctl.
       This is recommended in the Nextcloud documentation, because clients might behave
       unexpectedly, if they don't get a proper maintenance response due to missing/wrong endpoints during upgrade.
       Note that the script has to be run with root privileges to stop and start the webserver.
       Either this option or -k need to be passed to the script.
-z  | --zip
       use zip to compress backup folder instead of creating a gzipped tarball (tar.gz)
```

## Get in touch

[Create an issue](https://codeberg.org/BernieO/ncupgrade/issues/new) or send an [email](mailto:bernieo.code@gmx.de).

## Donations

*ncupgrade* is and will remain free open source software.  
If you find *ncupgrade* useful, you could support the developer by making a donation. Any amount is appreciated!

- IBAN: `DE58 1203 0000 1032 8146 40` (BIC: `BYLADEM1001`)
- PayPal (Friends): `bernieo.code@gmx.de`

## License

Copyright (C) 2018 Bernhard Ostertag [bernieo.code@gmx.de](bernieo.code@gmx.de)
```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

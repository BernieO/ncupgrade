## v4.6.0 (15.09.2024)

- *added*
  - support for Nextcloud 30
  - perform mimetype migrations for Updates to Nextcloud 28 >= 28.0.9 and Nextcloud >= 29.0.3
- *fixed*
  - Nextcloud 29 also supports PHP 8.0
  - only verify checksum of nextcloud archive and ignore other checksums

## v4.5.0 (23.04.2024)

- *added*
  - support Nextcloud 29

## v4.4.0 (29.01.2024)

- *fixed*
  - revert change in v4.3.0 and get Nextcloud PGP key from nextcloud.com (solves issue #10)

## v4.3.1 (02.01.2024)

- *fixed*
  - Nextcloud 28 also supports PHP 8.3
- *improved*
  - dont check database requirements, if there is no change to previous Nextcloud version

## v4.3.0 (12.12.2023)

- *added*
  - support Nextcloud 28
- *improved*
  - get Nextcloud PGP key directly from keys.opengpg.org

## v4.2.0 (15.06.2023)

- *added*
  - support Nextcloud 27
- *improved*
  - make shellcheck happy

## v4.1.0 (22.03.2023)

- *added*
  - support Nextcloud 25 and Nextcloud 26

## v4.0.0 (06.11.2022)

- *removed*
  - branch `master` removed from repository (deprecated since `v3.0.0`, 06.05.2021)

## v3.4.0 (23.05.2022)

- *added*
  - support Nextcloud 24

## v3.3.0 (01.09.2021)

- *fixed*
  - Nextcloud 21, 22 and 23 do also support PHP 8.0

## v3.2.0 (04.07.2021)

- *fixed*
  - use `APCu` for the php command line interface, if configured in `config.php`

## v3.1.0 (27.05.2021)

- *improved*
  - README.md now contains a section with detailed instructions on how to migrate from GitHub to Codeberg

## v3.0.0 (06.05.2021)

- *added*
  - new option `-php|--php-engine` to make the used php command customizable (issue #8)
  - new option `-t|--tar` to use simple tar as archive method (issue #7)
  - add sections [Get in touch](README.md#get-in-touch), [Donations](README.md#donations) and [License](README.md#license) to README files
- *changed*
  - convert `changelog` to markdown to increase readability (`changelog` -> `changelog.md`)
  - use `stable` as default branch
- *deprecated*
  - branch master is deprecated
- *fixed*
  - also check for requirements before upgrading when using option `-utv`
  - a column needs to be added when upgrading from 17.0.X to 18.0.10 or 18.0.11
  - do not report an error, if the temporary directory has not yet been created
- *improved*
  - use traps to be able to clean up before exiting on error or termination
  - some code simplifications
  - catch nextcloud download server errors
  - user interaction for performing manual steps is not needed anymore
  - minor corrections in inline comments

## v2.0.0 (06.03.2021)

- *added*
  - simple process indicator to give feedback that a process isn't stalled (issue #6)
  - check for required version of MySQL/MariaDB for Nextcloud 21
- *improved*
  - support datafolder on nfs share (`no_root_squash`/`maproot` needed on client (issue #5))
  - support restoring backups to a different location
    (if the according database user does not exist yet in the database, it will not be created)
  - simplify some texts and inline comments
  - merge doubled code for retrieving database details into single function
  - review and update of `README.md`

## v1.0.0 (23.02.2021)

- *added*
  - possibility to process a file with commands when restoring a backup (option `-hs|--hook-script`)
- *fixed*
  - ignore option `-apf|--adjust-permissions-fast`, if not being run as root
- *improved*
  - keep existing folders and only delete their content before restoring backup
  - verify compressed backup archives (issue #4)
  - processing of dialogues
  - process of restoring PostgreSQL database (issue #2)
  - beautify error, if connecting to Nextclouds' download server is not possible
  - remove temp directories from unsuccessful executions of ncupgrade

## v0.8.0 (24.11.2020)

- *added*
  - manual step to add missing primary keys to tables for Nextcloud >= 20.0.2
  - new option to skip backup before upgrading (to speed up some testing scenarios)
- *improved*
  - print notice about adjusting file permissions fast, when using option `-apf`

## v0.7.0 (25.09.2020)

- *added*
  - manual step to add optional database columns for upgrade to Nextcloud 19
- *improved*
  - check for required version of PHP

## v0.6.0 (02.02.2020)

- *added*
  - option `-apf|--adjust-permissions-fast` to adjust file permissions faster
- *fixed*
  - create backup folder in case it is not yet existing
  - react accordingly, if restoring a PostgreSQL database returns an error
- *improved*
  - various code optimizations to increase robustness and speed
  - print message about unrecognized option within output frame
  - clean up temporary files in case of an error

## v0.5.0 (29.01.2020)

:house: *ncupgrade* moved to [codeberg.org](https://codeberg.org)

- *fixed*
  - option `-h|--help` did not work correctly
- *improved*
  - use a more failsafe method for converting given paths to absolute paths
  - increased portability by using `printf` instead of `echo`

## v0.4.14 (29.01.2020)

:warning: This is the last version hosted on GitHub

- *changed*
  - just some text changes for announcing the housemoving to [codeberg.org](https://codeberg.org)

## v0.4.13 (11.10.2019)

- *fixed*
  - tolerate trailing slash for path to datadirectory in `config.php`

## v0.4.12 (27.08.2019)

- *improved*
  - use defaults for unset values in `config.php`

## v0.4.11 (06.08.2019)

- *fixed*
  - if path to nextcloud contained a space, missing apps could not be copied

## v0.4.10 (03.06.2019)

- *fixed*
  - eliminate syntax error due to accidentally added false character

## v0.4.9 (17.05.2019)

- *improved*
  - check for PHP >= 7.1 before upgrading to Nextcloud 16

## v0.4.8 (05.05.2019)

- *fixed*
  - use appropriate character set for backing up MySQL/MariaDB
- *improved*
  - follow symlinks when archiving files
  - allow upgrades from release candidates

## v0.4.7 (25.01.2019)

- *improved*
  - add semicolon at end of SQL statements
  - remove comments about restoring PostgreSQL database due to fix in Nextcloud documentation

## v0.4.6 (04.01.2019)

- *fixed*
  - also switch to bigint in the filecache table when using SQLite as database

## v0.4.5 (11.12.2018)

- *fixed*
  - also recognise Nextcloud, if only lowercased in user documentation
- *improved*
  - PostgreSQL commands (backup/restore)

## v0.4.4 (06.10.2018)

- *improved*
  - use `/var/tmp/` instead of `/tmp` for temporary directory to not run out of space

## v0.4.3 (24.09.2018)

- *added*
  - if possible, disable maintenance mode in case of error
- *fixed*
  - restoring backup of PostgreSQL database failed, if no socket or port configured in `config.php`
  - error when using `utf8mb4` collation with MySQL eliminated
- *improved*
  - cleaner psql commands with revised comments
  - also perform manual steps when upgrading to Nextcloud > 13

## v0.4.2 (24.07.2018)

- *fixed*
  - backup of MySQL/PostgreSQL database failed, if no socket or port configured in `config.php`

## v0.4.1 (06.07.2018)

- *added*
  - print notice, if user running the script can't read the full path to necessary files
- *fixed*
  - proper handling of compression errors and database dump errors
  - don't interpret backslashes when reading input
  - don't use string comparison for comparing integers
  - use path for command to remove files
  - use double quotes to prevent globbing and word splitting
  - use globs instead of iterating over `ls` output

## v0.4.0 (07.06.2018)

- *added*
  - option `-ea|--exclude-apps` to prevent missing apps being copied from old to upgraded installation
  - option `-et|--exclude-themes` to prevent missing themes being copied from old to upgraded installation
- *fixed*
  - catching of error, if canonicalizing path was not possible

## v0.3.1 (02.04.2018)

- *fixed*
  - bug in restoring backup when using PostgreSQL

## v0.3.0 (28.03.2018)

- *added*
  - option `-l|--less-restrictive-permissions` to set less restrictive file permissions for shared hosting
  - option `-rb|--restore-backup` to restore a backup created with *ncupgrade*
  - manual steps for upgrade to Nextcloud 13
- *changed*
  - read Nextclouds configuration from `config.php` instead of using `occ`
- *fixed*
  - also run `occ upgrade`, if installed version is the same as previous one
  - minor bugfix in regular expressions
- *improved*
  - code readability

## v0.2.0 (23.03.2018)

- *added*
  - possibility to upgrade to a specific version (option `-utv`)
- *changed*
  - naming of backup files for better sorting in directory listings
- *fixed*
  - tolerate that Nextcloud 10.0.0 reports itself as 10.0

## v0.1.0 (19.03.2018)

:tada: Initial release of *ncupgrade*
